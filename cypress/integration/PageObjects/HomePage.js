class HomePage {

    navigateToQASandbox() {
        cy.get('#menu').click();
        cy.get("#keyboard_arrow_up").click();
        cy.wait(1000);
        cy.contains("QA Sandbox"). should('be.visible');
        cy.contains("QA Sandbox").should('contain', 'QA Sandbox').click();
    }
    navigateToCourseware() {
        cy.get("#menu").click();
        cy.wait(1000);
        cy.contains('Courseware').should('contain', 'Courseware').click();
        cy.wait(6000);
         }
    navigateToKnowledge() {
        cy.get("#menu").click();
        cy.wait(1000);
        cy.contains('knowledge').should('contain', 'Knowledge').click();
       }
    navigateToClasses() {
        cy.get("#menu").click();
        cy.wait(1000);
        cy.contains('Classes').should('contain', 'Classes').click();
        cy.get("#add").click();
        cy.wait(1000);
        }
    navigateToPlugins() {
        cy.get("#menu").click();
        cy.wait(1000);
        cy.contains('plugins').should('contain', 'Plugins').click();
        }

        
}
export default HomePage;
