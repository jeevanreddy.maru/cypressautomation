import 'cypress-xpath'
describe('Test Suite', () => {
    it('Creating class', () => {
        cy.visit('https://workspace-bronte-dev.pearson.com');
        cy.get("input[type='email']").type("shailaja.chundru@pearson.com");
        cy.get("input[type='password']").type("JOINED-snow-GUARD-MATTER");
        cy.contains('Login').click();
        cy.get('#menu').click();
        cy.wait(1000);
        cy.get("#keyboard_arrow_up").click();
        cy.wait(1000);
        cy.contains("QA Sandbox").click();
        cy.get('#menu').click();
        cy.wait(1000);
        cy.contains('Classes').click();
        cy.get("#add").click();
        cy.wait(1000);
        cy.get('#className-input-3').type('Math 101');
        cy.get("#startDate-input-4").click();
        cy.get("abbr[aria-label='October 8, 2020']").click();
        cy.get('#pearsonProductIdPpid-input-5').type('TestID1');
        cy.contains('Open enrollment').click();
        cy.contains("Create class").click();
        cy.wait(1000);
        cy.contains('Math 101').click();
        cy.wait(1000);
        cy.contains('New Courseware').click();
        cy.contains('TestClass1').click();
        cy.wait(1000);
        cy.contains('Deploy').click();
        cy.wait(1000);
        cy.get('#more_vert').click();
        cy.wait(1000);
        cy.contains('Update Deployment').click();
    })
})
