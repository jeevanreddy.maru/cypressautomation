import LoginPage from '../PageObjects/LoginPage';

const Ip = new LoginPage();

before('Login to Pearson Project', function () {
    cy.fixture('example').then(function (data) {
        this.data = data;
        cy.visit('/');
        cy.url().should('include', 'https://workspace-bronte-qaint.pearson.com');
        cy.title().should('include', 'Pearson');
        Ip.fillEmail(this.data.email);
        Ip.fillPassword(this.data.password);
        Ip.submit();
    })
})
